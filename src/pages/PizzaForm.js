import Router from '../Router.js';
import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<label>
					Image :<br/>
					<input type="text" name="image" placeholder="https://source.unsplash.com/xxxxxxx/600x600">
					<small>Vous pouvez trouver des images de pizza sur <a href="https://unsplash.com/">https://unsplash.com/</a> puis utiliser l'URL <code>https://source.unsplash.com/xxxxxxx/600x600</code> où <code>xxxxxxx</code> est l'id de la photo choisie (celui dans la barre d'adresse)</small>
				</label>
				<label>
					Prix petit format :
					<input type="number" name="price_small" step="0.05">
				</label>
				<label>
					Prix grand format :
					<input type="number" name="price_large" step="0.05">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = this.element.querySelector('.pizzaForm');
		form.addEventListener('submit', event => {
			event.preventDefault();
			this.submit();
		});
	}

	submit() {
		// D.4. La validation de la saisie
		const input = this.getInput();
		// try {
		// 	input.forEach((v, k) => {
		// 		if (v === '') throw k;
		// 	});
		// } catch (k) {
		// 	alert(`Erreur : le champ ${k} est obligatoire`);
		// 	return;
		// }
		const pizza = Object.fromEntries(input);
		if (pizza.name == '') {
			alert('Le champ "Nom" est obligatoire');
			return;
		}
		alert(`La pizza ${pizza.name} a été ajoutée !`);
		fetch('http://localhost:8080/api/v1/pizzas', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(pizza),
		}).then(response => {
			if (!response.ok) {
				throw new Error(`Erreur : ${response.statusText}`);
			}
			Router.navigate('/');
		});
	}

	getInput() {
		const names = document.querySelectorAll('input');
		const pizza = new Map();
		names.forEach(e => {
			pizza.set(e.name, e.value);
		});

		return pizza;
	}
}
